<?php

declare(strict_types = 1);

namespace Vymakdevel\NetteBase\Classes;

class Cache extends \Nette\Caching\Cache
{

	/**
	 * Load from cache, if offset not exist insert it to cache and then return
	 *
	 * @param string|mixed   $name
	 * @param \callback $fallback
	 * @param mixed    $parameters
	 *
	 * @return mixed
	 */
	public function load($name, $fallback = null, $parameters = null)
	{
		$key = self::getNamesForCache($name, $parameters);

		return parent::load($key, $fallback);
	}

	/**
	 * Save data to cache
	 *
	 * @param string|mixed $name
	 * @param string|mixed $data
	 * @param mixed[]  $dependencies
	 * @param mixed  $parameters
	 *
	 * @return mixed
	 */
	public function save(
		$name,
		$data,
		array $dependencies = null,
		$parameters = null
	)
	{
		$key = self::getNamesForCache($name, $parameters);

		return parent::save($key, $data, $dependencies);
	}

	/**
	 * Cleans cache by tags
	 *
	 * @param string|mixed[]|null $tags
	 */
	final public function cleanByTags($tags = null): void
	{
		if (!isset($tags)) {
			$this->clean(
				[
					self::ALL => true,
				]
			);
		} else {
			$this->clean(
				[
					self::TAGS => (is_array($tags)) ? $tags : [$tags],
				]
			);
		}
	}

	/**
	 * Clean cache by priority
	 *
	 * @param int $priority
	 */
	final public function cleanByPriority(int $priority)
	{
		$this->clean(
			[
				self::PRIORITY => $priority,
			]
		);
	}

	/**
	 * Generate unique cache name by attributes
	 *
	 * @param string       $methodName
	 * @param mixed[]|string|null $parameters
	 *
	 * @return string
	 * @static
	 */
	public static function getNamesForCache(
		string $methodName,
		$parameters = null
	): string
	{
		if (empty($parameters)) {
			return $methodName;
		}

		if (is_array($parameters)) {
			foreach ($parameters as $value) {
				if (!empty($value)) {
					if (is_array($value)) {
						$methodName .= '_' . implode('_', $value);
					} else {
						$methodName .= '_' . $value;
					}
				}
			}
		} else {
			$methodName .= '_' . $parameters;
		}

		return hash('sha384', $methodName);
	}

}

<?php

declare(strict_types = 1);

namespace Vymakdevel\NetteBase\Control;

use Nette\Application\UI\ITemplate;
use Nette\Utils\Strings;

/**
 * Base control for each other control
 *
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
abstract class Control extends \Nette\Application\UI\Control
{

	use \Vymakdevel\NetteBase\Traits\TBaseControlPresenter {
		createTemplate as private myTemplate;
	}

	/** @var bool Check if is update */
	public $update;

	/**
	 * Return control name
	 *
	 * @return string
	 */
	private function getControlName(): string
	{
		return $this->getReflection()->getShortName();
	}

	/**
	 * Return template file name
	 *
	 * @return string
	 */
	private function getControlTemplateName(): string
	{
		return Strings::firstLower(
			Strings::replace($this->getControlName(), '/Control/')
		);
	}

	/**
	 * Return control dir
	 *
	 * @return string
	 */
	private function getControlDir(): string
	{
		$path = $this->getReflection()->getFileName();

		return Strings::replace(
			$path,
			'/' . $this->getControlName() . '.php/'
		);
	}

	/**
	 * Return template dir for control
	 *
	 * @return string
	 */
	public function getTemplateDir(): string
	{
		return $this->getControlDir() . '/template';
	}

	/**
	 * Return path for template file
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	public function getTemplateFile(string $name): string
	{
		$replaced = Strings::replace($name, '/render/');

		return $this->getTemplateDir() . '/' . $this->getControlTemplateName()
			. $replaced . '.latte';
	}

	/**
	 * Create template and set file
	 *
	 * @return \Nette\Application\UI\ITemplate
	 */
	protected function createTemplate(): ITemplate
	{
		$template = $this->myTemplate();
		$path = $this->getTemplateDir() . '/' . $this->getControlTemplateName()
			. '.latte';

		if (file_exists($path)) {
			$template->setFile($path);
		}

		return $template;
	}

	/**
	 * Render template
	 */
	public function render()
	{
		$template = $this->template;
		if ($template->getFile()) {
			$template->render();
		} else {
			trigger_error(
				'Šablona pro komponentu ' . $this->getControlName()
				. ' nebyla nastavena',
				E_USER_ERROR
			);
		}
	}

	/**
	 * Redraw control
	 *
	 * @param string $name
	 * @param string $snippet
	 */
	protected function tryRedrawControl(string $name, ?string $snippet = null): void
	{
		try {
			$component = $this->presenter->getComponent($name, false);
		} catch (\Throwable $void) {
			return;
		}

		if ($component) {
			if (empty($snippet)) {
				$component->redrawControl();
			} else {
				$component->redrawControl($snippet);
			}
		}
	}

}

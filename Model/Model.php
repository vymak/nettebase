<?php

declare(strict_types = 1);

namespace Vymakdevel\NetteBase\Model;

/**
 * Base model
 *
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
abstract class Model
{

	use \Nette\SmartObject;

	/** @var \Nette\Database\Context Database connection */
	protected $db;

}

document.getElementById("eu-cookies-accept").onclick = function () {
    var date = new Date();
    date.setFullYear(date.getFullYear() + 10);
    document.cookie = 'eu-cookies=1; path=/; expires=' + date.toGMTString();
    document.getElementById("eu-cookies").style.display = 'none';
};
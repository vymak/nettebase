<?php

declare(strict_types = 1);

namespace Vymakdevel\NetteBase\Components;

use Nette\Application\UI\ITemplate;

class EuCookiesControl extends \Vymakdevel\NetteBase\Control\Control
{

	/** @var bool */
	private $generateJsInTemplate = false;

	protected function createTemplate(): ITemplate
	{
		$template = parent::createTemplate();
		$template->setFile(__DIR__ . '/templates/euCookies.latte');
		$template->cookies = filter_input(INPUT_COOKIE, 'eu-cookies');
		$template->generateJs = $this->generateJsInTemplate;

		return $template;
	}

	public function setGenerateJs(bool $value)
	{
		$this->generateJsInTemplate = $value;
	}

}

<?php

declare(strict_types = 1);

namespace Vymakdevel\NetteBase\Traits;

use Latte\Engine;
use Latte\Loaders\StringLoader;
use Nette\Application\UI\ITemplate;
use Nette\Application\UI\Multiplier;
use Nette\Bridges\ApplicationLatte\UIMacros;
use Nette\Bridges\FormsLatte\FormMacros;
use Nette\Reflection\ClassType;
use Nette\Utils\Strings;

const SUCCESS = 'success';
const INFO = 'info';
const NOTICE = 'warning';
const ERROR = 'danger';

/**
 * Trait for controls and presenters
 *
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 */
trait TBaseControlPresenter
{

	/**
	 * Create templates, add helpers and return template
	 *
	 * @param mixed $class
	 *
	 * @return \Nette\Application\UI\ITemplate
	 */
	protected function createTemplate($class = null): ITemplate
	{
		$template = parent::createTemplate($class);
		$template->getLatte()->addFilter(null, '\App\Classes\Helpers::common');

		return $template;
	}

	/**
	 * Automatic create control
	 *
	 * @param string|mixed $name
	 *
	 * @return mixed
	 */
	protected function createComponent($name)
	{
		$method = 'createComponent' . Strings::firstUpper($name);
		if (method_exists($this, $method)) {
			return $this->$method($name);
		}

		$className = '\\' . Strings::firstUpper($name) . 'Control';

		$parentNamespace = Strings::replace(
			$this->getReflection()->getNamespaceName(),
			'/[\\\]Presenters/'
		);
		$namespaces = [
			'App\Components',
			$parentNamespace . '\Control',
			$parentNamespace,
		];

		$namespace = false;
		foreach ($namespaces as $item) {
			if (class_exists($item . $className)) {
				$namespace = $item;
				break;
			}
		}

		if ($namespace) {
			$class = $namespace . $className;
			$controlReflection = new ClassType($class);
			$haveAnnotations = $controlReflection->hasAnnotation(
				'multiControl'
			);

			$var = 'components' . Strings::firstUpper($name);
			if (property_exists($this, $var)) {
				if ($haveAnnotations) {
					return new Multiplier(
						function ($itemId) use ($var) {
							return $this->$var->create($itemId);
						}
					);
				}

				return $this->$var->create();
			}

			if ($haveAnnotations) {
				return new Multiplier(
					function ($itemId) use ($class) {
						return new $class($itemId);
					}
				);
			}

			return new $class;
		}

		return parent::createComponent($name);
	}

	/**
	 * Add flash message to list of messages
	 *
	 * @param string|mixed  $text
	 * @param bool $success
	 * @param string|mixed  $redirect
	 * @param string|mixed  $type
	 */
	final public function flashMessage(
		$text,
		$success = null,
		$redirect = null,
		$type = NOTICE
	)
	{
		if (isset($success)) {
			$type = ($success) ? SUCCESS : ERROR;
		}

		parent::flashMessage($text, $type);

		if (isset($redirect)) {
			$this->redirect($redirect);
		}
	}

	/**
	 * Return HTML code from template and data
	 *
	 * @param string $template
	 * @param mixed[]  $data
	 *
	 * @return string
	 */
	protected final function getHtmlFromTemplate(
		string $template,
		array $data
	): string
	{
		$latte = $this->getLatteEngine();

		$data['basePath'] = $this->presenter->getHttpRequest()->getUrl()->getBasePath();
		$data['baseUri'] = $this->presenter->getHttpRequest()->getUrl()->hostUrl;

		return $latte->renderToString($template, $data);
	}

	/**
	 * Return HTML code from string template and data
	 *
	 * @param string $data
	 * @return string
	 */
	protected final function getHtmlFromStringTemplate(string $data): string
	{
		$latte = $this->getLatteEngine();
		$latte->setLoader(new StringLoader);

		$params['basePath'] = $this->presenter->getHttpRequest()->getUrl()->getBasePath();
		$params['baseUri'] = $this->presenter->getHttpRequest()->getUrl()->hostUrl;
		$result = $latte->renderToString($data, $params);

		return $result;
	}

	/**
	 * Create and return Latte instance
	 *
	 * @return \Latte\Engine
	 */
	private function getLatteEngine(): Engine
	{
		$latte = new Engine;
		$latte->addProvider('uiControl', $this);
		$latte->addProvider('uiPresenter', $this->presenter);

		$latte->setTempDirectory(__DIR__ . '/../../../../temp/cache/myLatte');
		$latte->onCompile[] = function ($latte) {
			UIMacros::install(
				$latte->getCompiler()
			);
			FormMacros::install($latte->getCompiler());
		};
		$latte->addFilter(null, '\App\Classes\Helpers::common');

		return $latte;
	}

}
